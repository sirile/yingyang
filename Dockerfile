# Application to build
ARG appname=yingyang

# Build
FROM golang:alpine AS build-source
ARG appname
WORKDIR /app
COPY ./${appname}.go .
COPY ./go.mod .
RUN CGO_ENABLED=0 go build -ldflags="-s -w" ${appname}.go

# Compress
FROM alpine:latest AS compressor
ARG appname
RUN apk add upx
WORKDIR /app
COPY --from=build-source /app/${appname} /app/${appname}
RUN upx --brute /app/${appname}

# Final
FROM scratch
ARG appname
EXPOSE 8080
COPY --from=compressor /app/${appname} executable
CMD ["./executable"]
